# Introduction to Node.js


#VSLIDE
## Who am I
![Image](./assets/ava_0043.jpg)

Alexander Zaycev

JavaScript Developer  
<small>with 6 years experience</small>

<small>Linux (Ubuntu) user =)</small>


#VSLIDE
## My profiles
+ https://github.com/nervgh
+ http://javascript.ru/forum/members/17434-nerv_/
+ https://htmlforum.io/profile/45648-nerv/






#HSLIDE
## Why Node.js?


#VSLIDE
### 1. Technological blow-up
is a significant IT challenge today


#VSLIDE
### From that follows
1. Developers should know more and more new tools, keep their knowledges up to date
2. Recruiters should find people who have the unique knowledges

<small>It's hard work for each of them!</small>


#VSLIDE
### Conclusion #1
_В современном мире развитие технологий можно сравнить с процессом расширения Вселенной. Этот процесс подразумевает непрерывность и постоянное ускорение. В этих условиях гораздо проще поддерживать актуальность знаний на соответствующим уровне применительно к одному инструменту (языку/технологии), чем к нескольким._


#VSLIDE
### Conclusion #2
_Разнообразие инструментов хорошо для совершенствования этих инструментов путем эволюции, но, зачастую, губительно для отдельно взятого проекта._


#VSLIDE
### 2. JavaScript like the universal tool
It is the one most wide used languages in the world*

<small>*on the end of 2016 year by the GitHub stat data</small>


#VSLIDE
### Area of use of JavaScript
1. Веб-приложения
  + AJAX
  + Comet
  + Браузерные операционные системы
2. Букмарклеты
3. Пользовательские скрипты в браузере

<small>https://ru.wikipedia.org/wiki/JavaScript</small>


#VSLIDE
### Area of use of JavaScript
<ol start="4">
  <li>Серверные приложения</li>
  <li>Мобильные приложения</li>
  <li>Виджеты</li>
  <li>Прикладное программное обеспечение</li>
  <li>Манипуляция объектами приложений</li>
<ol>

<small>https://ru.wikipedia.org/wiki/JavaScript</small>


#VSLIDE
### Area of use of JavaScript
<ol start="9">
  <li>Офисные приложения</li>
  <ul>
    <li>Microsoft Office</li>
    <li>OpenOffice.org</li>
  </ul>
  <li>Обучение информатике</li>
<ol>

<small>https://ru.wikipedia.org/wiki/JavaScript</small>


#VSLIDE
### Development of web-applications
JavaScript-devs can code
1. Frontend
2. Backend
3. Full-stack (if he/she has enough skills)


#VSLIDE
### Who is a full-stack developer?
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="401px" height="351px" version="1.1" content="&lt;mxfile userAgent=&quot;Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/55.0.2883.87 Chrome/55.0.2883.87 Safari/537.36&quot; version=&quot;6.1.2&quot; editor=&quot;www.draw.io&quot; type=&quot;google&quot;&gt;&lt;diagram name=&quot;Page-1&quot;&gt;7VjBcpswEP0ajpkBBBiOthO3l3SmTWd6FkiAJgJRWY6dfn1XIGxjlBkOkFyMD0ZvFwm9fSutcNC2On2TuCmfBaHc8V1yctCj4/ue50bwp5H3DoncsAMKyYhxugAv7B81oGvQAyN0P3BUQnDFmiGYibqmmRpgWEpxHLrlgg9HbXBBR8BLhvkY/cOIKjs0Dt0L/p2yolTnCRtLirPXQopDbcZzfJS3V2eucN+X8d+XmIjjFYSeHLSVQqjurjptKdfc9rR1z+0+sJ7fW9JaTXnAD1KSp8hbhQQlBLkPpoc3zA+0n0L7ouq9J+dYMkVfGpzp9hEE4KBNqSoOLQ9uc8b5VnAhW2+Uh/oH+F5J8UqvLFF7gcUMSaWipw/n4Z3ZAdVRUVEl38Hl1M/DEGoE5/Xt4yV8QR+k8ip0qI8pNpIpzn1faIMbw9xEFv25WSSYxnlmZTGLaZrPxGJ0w2I0ZhFFFhaDJUhEs5MY0pgENhJjP0WzSfGGRN+Lp5GIogVIDGbP5zijmVWJaRwGkE3LkLiaqMRkCSWGc5NIPdDiykZiEq0QXkiJaBVOTGd/ARIjC4kRV5ocAfOBXbojD9C/B73/bbbiIBmVYPpBjxd4QHwP6j4e9m0hsQYH329OrevloajQ/490z4q6Hxkm0Q3eGUdRBe7VMJDDgNWipjfRNRDmehj0mEGsYApooyPJoMJYG0PFCNHDbGzK0S+1wxXjOnK/WQW1kCHB/SUqXBsXUzhpDcyhF5TcLP+WpPNsm6i/RNJ5tlJkWcGM1PKM5euhuatlilpguflKtdhKrk9Wy062I+02cAh4oHAEgAMQfaNcNBVtZXNX0XiPisOhimJvvEd5FhWtFlGRreb8ZBX9XN+VYl1v4nhYzcRfut7MXhPmeZYlia0mRBFKEFmmJgzQ1CPeEjWhN2NR2CXJmlSsZkAhVkx8XOd1cCrvuWXdy4NgvArPlVvQvHzfam1XHxHR038=&lt;/diagram&gt;&lt;/mxfile&gt;" style="background-color: rgb(255, 255, 255);"><defs/><g transform="translate(0.5,0.5)"><rect x="0" y="0" width="400" height="350" fill="#f5f5f5" stroke="#666666" pointer-events="none"/><rect x="20" y="20" width="360" height="40" fill="#dae8fc" stroke="#6c8ebf" pointer-events="none"/><rect x="20" y="78" width="360" height="36" fill="#d5e8d4" stroke="#82b366" pointer-events="none"/><rect x="20" y="130" width="360" height="90" fill="#f8cecc" stroke="#b85450" pointer-events="none"/><rect x="20" y="235" width="360" height="42" fill="#e1d5e7" stroke="#9673a6" pointer-events="none"/><g transform="translate(159.5,26.5)"><switch><foreignObject style="overflow:visible;" pointer-events="all" width="80" height="26" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 22px; font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 80px; white-space: nowrap; word-wrap: normal; text-align: center;"><div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit;"><font face="Courier New" style="font-size: 22px">Design</font></div></div></foreignObject><text x="40" y="24" fill="#000000" text-anchor="middle" font-size="22px" font-family="Times New Roman">[Not supported by viewer]</text></switch></g><g transform="translate(159.5,82.5)"><switch><foreignObject style="overflow:visible;" pointer-events="all" width="80" height="26" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 22px; font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 80px; white-space: nowrap; word-wrap: normal; text-align: center;"><div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit;"><font face="Courier New" style="font-size: 22px">Markup</font></div></div></foreignObject><text x="40" y="24" fill="#000000" text-anchor="middle" font-size="22px" font-family="Times New Roman">[Not supported by viewer]</text></switch></g><g transform="translate(45.5,148.5)"><switch><foreignObject style="overflow:visible;" pointer-events="all" width="308" height="54" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 22px; font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 308px; white-space: normal; word-wrap: normal; text-align: center;"><div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit;"><font face="Courier New" style="font-size: 22px">Front/Back-end development</font></div></div></foreignObject><text x="154" y="38" fill="#000000" text-anchor="middle" font-size="22px" font-family="Times New Roman">[Not supported by viewer]</text></switch></g><g transform="translate(184.5,242.5)"><switch><foreignObject style="overflow:visible;" pointer-events="all" width="26" height="26" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 22px; font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 28px; white-space: nowrap; word-wrap: normal; text-align: center;"><div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit;"><font face="Courier New" style="font-size: 22px">QA</font></div></div></foreignObject><text x="13" y="24" fill="#000000" text-anchor="middle" font-size="22px" font-family="Times New Roman">[Not supported by viewer]</text></switch></g><rect x="20" y="290" width="360" height="42" fill="#ffcc99" stroke="#36393d" pointer-events="none"/><g transform="translate(107.5,297.5)"><switch><foreignObject style="overflow:visible;" pointer-events="all" width="184" height="26" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 22px; font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 186px; white-space: nowrap; word-wrap: normal; text-align: center;"><div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit;"><font face="Courier New">Administration</font><br /></div></div></foreignObject><text x="92" y="24" fill="#000000" text-anchor="middle" font-size="22px" font-family="Times New Roman">[Not supported by viewer]</text></switch></g></g></svg>


#VSLIDE
### Conclusion
_Для разработчика это означает, что:_
1. _независимо от того, что он разрабатывает (фронт или бэк) у него будет прокачиваться общий скилл по языку_
2. _это ведет к тому, что приложив минимум усилий можно переквалифицироваться из клиентского разработчика в серверного или наоборот_
3. _для тех разработчиков, которые "давно в теме", быть фулл-стеком_


#VSLIDE
### 3. ECMAScript & Node.js are mature enough
+ ECMAScript standards give us power: modules, classes, async/await, etc.
+ Node.js' package ecosystem, npm, is the largest ecosystem of open source libraries in the world*


<small>*by the data of https://nodejs.org</small>


#VSLIDE
### Conclusion
+ Node.js из хипстерской технологии трансформируется в инструмент для рядового серверного разработчика
+ На рынке начинает увеливаться спрос на Node.js разработчиков\* и на full-stack JavaScript разработчиков\*\*

<small>\*по моим скромным наблюдениям</small>  
<small>\*\*этот тренд только набирает обороты</small>






#HSLIDE
## What is Node.js?


#VSLIDE
## Node.js is ...
Node.js is a program platform based on V8 engine which turns JavaScript from single-purpose language to <u>multi-purpose</u> language.

<small>https://ru.wikipedia.org/wiki/Node.js</small>


#VSLIDE
### What is exactly mean "multi-purpose"?

+ Node.js gives us an ability to interact with I/O devices via its own API.
+ Also it allows us to include external libraries which written on another program languages and interact with them.

<small>https://ru.wikipedia.org/wiki/Node.js</small>


#VSLIDE
### Note
Node.js does not only for web-apps but we will focus on them in this presentation.






#HSLIDE
## How Node.js works


#VSLIDE
> I heard Node.js is single treaded.  
> What does it mean?


#VSLIDE
**Процесс** — абстракция ОС, которая инкапсулирует в себе ресурсы: открытые файлы, их дескрипторы, потоки и т.д. <u>Каждый процесс имеет как минимум один поток</u>. Также каждый процесс имеет свое собственное виртуальное адресное пространство, а потоки одного процесса разделяют адресное пространство процесса.

<small>https://habrahabr.ru/post/40227/</small>


#VSLIDE
**Поток/нить** — это, сущность операционной системы для исполнения программного кода. Общее назначение потоков — параллельное выполнение на процессоре двух или более различных задач.

<small>https://habrahabr.ru/post/40227/</small>  
<small>https://ru.wikipedia.org/wiki/Поток_выполнения</small>


#VSLIDE
### Why is Node.js single threaded? #1
> In PHP (or Java/ASP.NET/Ruby) based webservers every client request is instantiated on a new thread. But in Node.js all the clients run on the same thread (they can even share the same variables!) I understand that I/O operations are event based so they don't block the main thread loop...

<small>http://stackoverflow.com/q/17959663</small>


#VSLIDE
### Why is Node.js single threaded? #2
>... But it makes things difficult. For example I can't run a CPU intensive function because it blocks the main thread (and new client requests are blocked) so I need to spawn a process (which means I need to create a separate JavaScript file and execute another node process on it).


#VSLIDE
### V8 #1
V8 — движок JavaScript с открытым программным кодом, распространяемый по лицензии BSD. Разработан датским отделением компании Google.

<small>https://ru.wikipedia.org/wiki/V8_(движок_JavaScript)</small>


#VSLIDE
### V8 #2
V8 исполняет JavaScript-сценарии в особых «контекстах», которые по сути являются отдельными виртуальными машинами. 
Правда в одном процессе может работать только одна виртуальная машина, несмотря на возможность использования нескольких потоков. 
В Chromium это обходится мультипроцессовой архитектурой, повышающей также стабильность и безопасность, реализуя таким образом механизм «песочницы».


#VSLIDE
### What is Backend?
![backend](./assets/exskype-2-1024.jpg)
<small>the image from http://www.highload.ru/2014/abstracts/1639.html</small>


#VSLIDE
### What does Backend do? #1
![backend](./assets/exskype-3-1024.jpg)
<small>the image from http://www.highload.ru/2014/abstracts/1639.html</small>


#VSLIDE
### What does Backend do? #2
> Если посмотреть на соотношение скорости процессора и возможности сетевых соединений, то отличия — на пару порядков. Например, сжатие 1 Кб данных занимает 3 мкс, в то время как round-trip* в одну сторону даже внутри одного дата-центра — это уже 0,5 мс. 

<small>[Round-trip delay time, RTD](https://ru.wikipedia.org/wiki/Круговая_задержка) - время, затраченное на отправку сигнала, плюс время, которое требуется для подтверждения, что сигнал был получен</small>
<small>http://www.highload.ru/2014/abstracts/1639.html</small>


#VSLIDE
### What does Backend do? #3
> Любое сетевое взаимодействие, которое нужно бэкенду (например, отправка запроса в БД), потребует, как минимум 2х round-trip'ов и по сравнению с тем процессорным временем, которое он тратит на обработку данных, — это совершенно незначительно.

<small>http://www.highload.ru/2014/abstracts/1639.html</small>


#VSLIDE
### What does Backend do? #3
> Большую часть времени обработки запроса бэкенд ничего не делает, он ждет.

<small>http://www.highload.ru/2014/abstracts/1639.html</small>


#VSLIDE
### How do we evaluate backend performance?

+ RPS (requests per second)
+ Latency (response time)


#VSLIDE
### Which types of Network I/O do we know?

+ Blocking
+ Non-blocking


#VSLIDE
### How can we achieve high performance?

We need use <u>Multitasking</u> for:
+ increase RPS (requests per second)
+ decrease Latency (response time)


#VSLIDE
### Multitasking goals
![multitasking](./assets/exskype-15-1024.jpg)
<small>the image from http://www.highload.ru/2014/abstracts/1639.html</small>


#VSLIDE
### Kinds of multitasking

1. Многопроцессность
2. Многопоточность
3. Кооперативность


#VSLIDE
### Multi-processing
![multiprocessing](./assets/exskype-16-1024.jpg)
<small>the image from http://www.highload.ru/2014/abstracts/1639.html</small>


#VSLIDE
### Multi-threading
![Multithreaded](./assets/exskype-19-1024.jpg)
<small>the image from http://www.highload.ru/2014/abstracts/1639.html</small>


#VSLIDE
### Cooperation
![Cooperative](./assets/exskype-22-1024.jpg)
<small>the image from http://www.highload.ru/2014/abstracts/1639.html</small>






#HSLIDE
## What you should know before to code on Node.js


#VSLIDE
## ECMAScript 2015 standard
+ Promises
+ Modules*

<small>*also you should know CommonJS module syntax</small>  
<small>http://learn.javascript.ru/es-modern</small>


#VSLIDE
## Event Loop
![event_loop](./assets/event_loop.png)

<small>http://frontender.info/understanding-the-node-js-event-loop/</small>






#HSLIDE
## Node.js core concepts


#VSLIDE
### Non-blocking code
_Blocking_ methods execute _synchronously_ and _non-blocking_ methods execute _asynchronously_.

Blocking (bad)
```js
const fs = require('fs');
const data = fs.readFileSync('/file.md'); 
// blocks execution until file is read
```
Non-blocking (good)
```js
const fs = require('fs');
fs.readFile('/file.md', (err, data) => {
  if (err) throw err;
});
```

<small>https://nodejs.org/en/docs/guides/blocking-vs-non-blocking/</small>


#VSLIDE
### Observer pattern
and its implementation in Node.js -- EventEmitter

```js
const EventEmitter = require('events');

let emitter = new EventEmitter();

emitter.on('foobar', function(a, b, c) {
    console.log(a, b, c)
});

emitter.emit('foobar', 1, 2, 3) // -> 1, 2 ,3
```

<small>https://nodejs.org/api/events.html</small>


#VSLIDE
### Streams

```js
const fs = require('fs');

let readableStream = fs.createReadStream('file1.txt');
let writableStream = fs.createWriteStream('file2.txt');

readableStream.pipe(writableStream);
```

<small>https://www.sitepoint.com/basics-node-js-streams/</small>


#VSLIDE
### Semver & NPM

+ [NPM (Node.js Package Manager)](https://ru.wikipedia.org/wiki/NPM)
+ [SemVer](http://semver.org/)

<small>...if you really want to write web-apps</small>






#HSLIDE
## A "Hello World"<br>web application on Node.js


#VSLIDE
## Step #1
First of all, you need download and install Node.js.

https://nodejs.org/en/download/


#VSLIDE
## Step #2
Second, you need create a file `app.js`.


#VSLIDE
## Step #3
Third, you need copy the code from [Node.js site](https://nodejs.org/en/about/) and paste that code into your `app.js` file.


#VSLIDE
```js
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World\n');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```

<small>https://nodejs.org/en/about/</small>


#VSLIDE
## Step #4
Fourth, you need start your app:

```bash
# Don't forget specify path to your app.js

node app.js

# Server running at http://127.0.0.1:3000/
```






#HSLIDE
## What generally is a backend?


#VSLIDE
## Essentially web-server is

Framework + ORM/ODM + Template engine


#VSLIDE
Next we will talk only about Frameworks






#HSLIDE
## Express.js


#VSLIDE
## What is Express.js?
Express - это веб-фреймворк маршрутизации (routing) и промежуточной обработки (middleware) с минимальной собственной функциональностью: 
приложение Express, по сути, представляет собой серию вызовов функций промежуточной обработки.

<small>http://expressjs.com</small>


#VSLIDE
### Hello world example

```js
let express = require('express')
let app = express()

app.use(function (req, res, next) {
  console.log('Time:', Date.now())
  next()
})

app.use(function (req, res) {
  res.send('Hello World!')
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
```


#VSLIDE
### Routing with Express.js
```js
// GET method route
app.get('/', function (req, res) {
  res.send('GET request to the homepage')
})

// POST method route
app.post('/', function (req, res) {
  res.send('POST request to the homepage')
})

// With params
app.get('/users/:userId', function (req, res) {
  res.send(req.params) // {userId:String}
})
```





#HSLIDE
## Middleware<br>design pattern


#VSLIDE
### What is middleware?
It's similar to a pipeline

```bash
cmd1 | cmd2 | .... | cmdN
```


#VSLIDE
Essentially, it is the queue of functions:

```js
app.middleware = [fn1, fn2, fn3, ...];
```

#VSLIDE
When a request comes to the server we run them sequentially:

```js
// 1. Созать Request и Response объекты
// 2. Отправить их в каждую из функций

app.use(function(req, res, next) { 
  // Эта функция и есть middleware (!)
  // 3. Если нужно выполнить следующую функцию в очереди, 
  // вызываем next()
  next()
  // 4. Иначе обработка запроса прекратиться на этой функции
})
```






#HSLIDE
## The Evolution of Asynchronous JavaScript


#VSLIDE
### Callbacks
```js
// Express
app.use(function(req, res, next) {
  User.findById(id, function(err, result) {
    // do something with result
    res.send(result) // and send it
  })
})
```


#VSLIDE
### Promises
```js
// Express
app.use(function(req, res, next) {
  User.findById(id).then(function(result) {
    // do something with result
    res.send(result) // and send it
  })
})
```


#VSLIDE
### Generators+Promises
```js
// Koa@1
app.use(function* (next) {
  let result = yield User.findById(id)
  // do something with result
  this.body = result // and send it
  // this is koa' context
  // this.request
  // this.response
})
```


#VSLIDE
### Async/Awaits+Promises
```js
// Koa@2
app.use(async function (ctx, next) {
  let result = await User.findById(id)
  // do something with result
  ctx.body = result // and send it
  // ctx is koa' context
  // ctx.request
  // ctx.response
})
```






#HSLIDE
## Koa.js


#VSLIDE
### What is Koa.js?

Koa is a new web framework designed by the team behind Express, which aims to be a smaller, more expressive, and more robust foundation for web applications and APIs.

<small>http://koajs.com/</small>


#VSLIDE
### Comparison with Express #1
Philosophically, Koa aims to "fix and replace node", whereas Express "augments node".
Koa uses promises and async functions to rid apps of callback hell and simplify error handling.
It exposes its own `ctx.request` and `ctx.response` objects instead of node's `req` and `res` objects.


#VSLIDE
### Comparison with Express #2
Express, on the other hand, augments node's `req` and `res` objects with additional properties and methods
and includes many other "framework" features, such as routing and templating, which Koa does not.


#VSLIDE
### Comparison with Express #3
Thus, Koa can be viewed as an abstraction of node.js's `http` modules, where as Express is an application framework for node.js.


#VSLIDE
### Comparison with Express #4

| Feature           | Koa | Express | Connect |
|------------------:|-----|---------|---------|
| Middleware Kernel | ✓   | ✓       | ✓       |
| Routing           |     | ✓       |         |
| Templating        |     | ✓       |         |
| Sending Files     |     | ✓       |         |
| JSONP             |     | ✓       |         |


#VSLIDE
### Comparison with Express #5

| Framework |   Since   | Till |
|:----------|:---------:|:----:|
| Connect   | May, 2010 | Now  |
| Express   | Jun, 2009 | Now  |
| Koa       | Aug, 2013 | Now  |


#VSLIDE
### Why you should consider Koa instead of Express with Node.js

+ [Why you should consider Koa instead of Express with Node.js](https://medium.com/@l1ambda/why-you-should-use-koa-with-node-js-7c231a8174fa)
+ [Why You Should and Shouldn't Use Koa](http://www.jongleberry.com/why-you-should-and-shouldnt-use-koa.html)
+ [Koa vs Express](https://github.com/koajs/koa/blob/master/docs/koa-vs-express.md)


#VSLIDE
### Difference "control the flow"

| Framework | Flow control method |
|:---------:|:-------:|
| Express | Callbacks / Promises |
| Koa@1 | Generators + Promises |
| Koa@2 | Async/Await + Promises |


#VSLIDE
### Middleware compatibility
+ You can [convert](https://github.com/koajs/convert) koa@1 middleware to koa@2 middleware
+ You can [convert](https://github.com/vkurchatkin/koa-connect) express middleware to koa middleware


#VSLIDE
Next we will talk only about koa@2






#HSLIDE
## Koa@2 examples


#VSLIDE
To run examples we need:
+ `node >=7.6`
+ or `node 7` with `--harmony-async-await` flag


#VSLIDE
### Hello World
```js
const Koa = require('koa')

let app = new Koa()

app.use(function(ctx) {
  ctx.body = 'Hello Koa'
})

app.listen(3000)
```

#VSLIDE
### Context Explanation
```js
app.use(function(ctx) {
  ctx // is the Context
  ctx.request // is a koa Request
  ctx.response // is a koa Response
  
  // ctx.body === ctx.response.body
})
```


#VSLIDE 
You can found more info about Koa' context here -- http://koajs.com/#context


#VSLIDE
### JSON response
```js
const Koa = require('koa')

let app = new Koa()

app.use(function(ctx) {
  ctx.body = [1,2,3] // <-- any object here
  
  // We can set {String|Buffer|Stream|Object|null} as a response
})

app.listen(3000)
```


#VSLIDE
### Multiple middleware
```js
const Koa = require('koa')

let app = new Koa()

// First middleware
app.use(function(ctx, next) {
  ctx.body = '1' // do something here
  next() // and go to next middleware
})
// Second middleware
app.use(function(ctx) {
  ctx.body += '1' // do something here end exit
})

app.listen(3000)
```


#VSLIDE
### Async function as middleware
```js
const Koa = require('koa')

let app = new Koa()

// If we need wait any operations we can use async functions:
app.use(async function(ctx) {
  let user = await User.findByName('Arnold')
  let permissions = await Permissions.check(user)
  ctx.body = {user,permissions}
})

app.listen(3000)
```


#VSLIDE
Koa is a middleware framework that can take 3 different kinds of functions as middleware:
+ common function
+ generator function (will be removed in koa@3)
+ async function

<small>https://github.com/koajs/koa#middleware</small>


#VSLIDE
### Error handler #1
```js
const Koa = require('koa')
let app = new Koa()

// Error handler
app.use(function(ctx, next) {
  try {
    await next()
  } catch(err) {
    console.log('Catched:', err)
  }
})
// Any middleware which throws an error
app.use(function(ctx) {
  throw Error('Ooops!')
})
app.listen(3000)
```


#VSLIDE
That is possible because Koa uses cascading

<small>http://koajs.com/#cascading</small>


#VSLIDE
### Composition #1

```js
const Koa = require('koa')
const mount = require('koa-mount') // koa-mount@^2.0.0

let app = new Koa()

// We can create a couple of apps 
// which handle certain requests and mount them
let foo = new Koa()
let bar = new Koa()
```


#VSLIDE
### Composition #2
```js
foo.use(function (ctx) {
  ctx.body = 'foo'
})

bar.use(function (ctx) {
  ctx.body = 'bar'
})

app.use(mount('/foo', foo)) // <-- will handle /foo requests
app.use(mount('/bar', bar)) // <-- will handle /bar requests

app.listen(3000)
```


#VSLIDE
### Routing
```js
const Koa = require('koa')
const Router = require('koa-trie-router') // koa-trie-router@^2.1.1

let app = new Koa()
let router = new Router()

router.get('/foo', function(ctx) {
  ctx.body = 'GET /foo'
})

app.use(router.middleware()) // <-- will handle /foo requests

app.listen(3000)
```


#VSLIDE
### RESTful #1
```js
const Koa = require('koa')
const Router = require('koa-trie-router') // koa-trie-router@^2.1.1

let app = new Koa()
// @see http://docs.phalconphp.ru/ru/latest/reference/tutorial-rest.html
let router = new Router()

// We could define simple helper:
router.loadRESTful = function (descriptor) {
  for(let key in descriptor) {
    let str = key.toLowerCase()
    let fn = descriptor[key]
    let [method, route] = str.split(' ')
    this[method](route || '/', fn)
  }
}
```


#VSLIDE
### RESTful #2
```js
// And use it like this
router.loadRESTful({
  'GET': middleware,
  'GET /search': middleware,
  'GET /:id': middleware,
  'POST': middleware,
  'PUT /:id': middleware,
  'DELETE /:id': middleware
})
// Demo middleware
function middleware(ctx) {
  let {method,url,params} = ctx
  ctx.body = `${method} ${url} ${JSON.stringify(params)}`
}
app.use(router.middleware())
app.listen(3000)
```






#HSLIDE
## Official and third-party middleware for Koa


#VSLIDE
+ [koa-trie-router](https://github.com/koajs/trie-router) -- Trie routing for Koa based on routington.
+ [koa-cash](https://github.com/koajs/cash) -- HTTP response caching for Koa. Caches the response based on any arbitrary store you'd like
+ [koa-passport](https://github.com/rkusa/koa-passport) -- Passport middleware for Koa
+ [koa-rbac](https://github.com/yanickrochon/koa-rbac) -- Role-Based Access Control for Koa
+ [koa-mount](https://github.com/koajs/mount) -- Mount other Koa applications as middleware / **HMVC**
+ [koa-bundle](https://github.com/koajs/bundle) -- Generic asset pipeline with caching, etags, minification, gzipping and sourcemaps.


#VSLIDE
+ [koa-lusca](https://github.com/koajs/koa-lusca) -- Web application security middleware for koa.
+ [koa-static](https://github.com/koajs/static) -- Koa static file serving middleware
+ [koa-sendfile](https://github.com/koajs/sendfile) -- Basic file-sending utility for koa
+ [koa-session](https://github.com/koajs/session) -- Simple cookie-based session middleware for Koa.
+ [koa-generic-session](https://github.com/koajs/generic-session) -- koa session store with memory, redis or others
+ koa-nunjucks-renderer -- A middleware for rendering templates using Nunjucks

#VSLIDE
+ [koa-compress](https://github.com/koajs/compress) -- Compress middleware for Koa
+ [koa-favicon](https://github.com/koajs/favicon) -- Koa middleware for serving a favicon. Based on serve-favicon
+ [koa-bodyparser](https://github.com/koajs/bodyparser) -- A body parser for koa
+ [koa-logger](https://github.com/koajs/logger) -- Development style logger middleware for Koa.
+ [koa-analytics](https://github.com/stevenmiller888/koa-analytics) -- Easily embed analytics into your Koa application.
+ [koa-ratelimit](https://github.com/koajs/ratelimit) -- Rate limiter middleware for koa


#VSLIDE
Need more middleware?  
See https://github.com/koajs/koa/wiki






#HSLIDE
## Cookbook


#VSLIDE
### Loading routes from a module #1
Assume, we have a module which exports routes:

**routes.js**
```js
exports.get = {
  index: function() {  // curl http://localhost/index
    // code here
  },
  about: function () { // curl http://localhost/about
    // code here
  }
}

exports.post = {
 feedback: function () { // curl -X POST http://localhost/feedback
    // code here
 }
}
```


#VSLIDE
### Loading routes from a module #2
And we want to load all our routes from that file.  
We could use simple helper for this:

```js
const Router = require('koa-trie-router')

let router = new Router()
// the helper
router.loadRoutesFromModule = function (module) {
  for(let verb in module) {
    for(let action in module[verb]) {
      let route = action.startsWith('/') ? action : '/' + action
      route = route === '/index' ? '/' : route
      this[verb](route, module[verb][action])
    }
  }
}
router.loadRoutesFromModule(require('routes')) // <-- LOADING
```


#VSLIDE
### Testing #1
Assume, we have a simple app like this:

**app.js**
```js
const Koa = require('koa')

let app = new Koa()

app.use(function(ctx) {
  ctx.body = 'Hello Koa@2'
})

module.exports = app
```


#VSLIDE
### Testing #2
+ `app.js` should exports our app
+ `app.js` shouldn't does `app.listen(port)`


#VSLIDE
### Testing #3
```js
const assertRequest = require('assert-request')
const app = require('app')

let request = assertRequest(app.listen())

describe('app', function() {
  it('should works', function () {
    return request
    .get('/')
    .status(200)
    .body('Hello Koa@2')
  })
})
```


#VSLIDE
### Testing #4

See more examples there -- https://github.com/koajs/trie-router/tree/master/test

<small>https://github.com/koajs/koa/issues/703</small>






#HSLIDE
## Coroutines. Vocabulary


#VSLIDE
### All they are different names...
+ [Green threads](https://ru.wikipedia.org/wiki/Green_threads) 
+ [Co + Generators](http://tobyho.com/2015/12/27/promise-based-coroutines-nodejs/)
+ [Flat async code](http://learn.javascript.ru/generator#плоский-асинхронный-код)
+ Async/Await
+ Fibers

...for [Coroutines](https://habrahabr.ru/post/253347/)






#HSLIDE
## Few helpful NPM packages<br>for Node.js developmnet


#VSLIDE
+ [config](https://github.com/lorenwest/node-config) -- An easy way manage configurations of your app.
+ [nodemon](https://github.com/remy/nodemon) -- Monitor for any changes in your node.js application and automatically restart the server.
+ [pm2](https://github.com/Unitech/pm2) -- Production process manager for Node.js apps with a built-in load balancer.






#HSLIDE
## Thank you for your attention!
